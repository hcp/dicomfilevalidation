package org.nrg.validation.dicom;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.xml.sax.SAXException;

public class XnatValidator {
	
	  protected static XnatImagesessiondataBean GetImageSession(String pathToXML) throws FileNotFoundException, SAXException, IOException{
		   XnatImagesessiondataBean imageSession = null;
	       File f = new File(pathToXML);
	       InputStream fis = new FileInputStream(f);
	       XDATXMLReader reader = new XDATXMLReader();
	       BaseElement base = reader.parse(fis);
	       try {
	    	   fis.close();
	       }catch(Exception e){}
		   return (XnatImagesessiondataBean)base;
	   }

}
