package org.nrg.validation.dicom;
import java.io.IOException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author Mohana Ramaratnam
 *
 */
public class XnatDicomTagValidator {
	
	private static final String COMPONENT_SEP = ";XNAT;"; 
	
	/*
	 * String dicomDumpURL = "XNAT_HOST_HERE/REST/services/dicomdump?src=/archive/projects/HCPEP-BWH-QC/experiments/PNL_E00001/scans/7&field=00181000&format=JSON"
	 * String tagValue = Dicom Field Value : 66109
	 * String user : XNAT User Name
	 * String password : XNAT Password
	 */
	
	public static boolean CheckTag(String host, String projectId, String expId, String scanId, String dicomTag, String tagValue, String user, String password) throws IOException{
		boolean tagValueAsExpected = false;
		String dicomDumpURL = host + "/REST/services/dicomdump?src=/archive/projects/" + projectId + "/experiments/" + expId + "/scans/" + scanId + "&field=" +  dicomTag + "&format=JSON";
		//Get value returned by the URL; Check the value against tagValue
		ResponseEntity<String> response = GetResponse(dicomDumpURL, user, password);
		if (response.getStatusCode().equals(HttpStatus.OK)) {
			final String responseBody = response.getBody();
			if (responseBody!=null && responseBody.length()>0) {
				String analysis = CheckValue(responseBody, tagValue);
				String[] parts = analysis.split(COMPONENT_SEP);
				tagValueAsExpected = false;
				if (parts[0] == "false") {
					tagValueAsExpected = false;
				}else {
					tagValueAsExpected = true;
				}
			}
		}

		return tagValueAsExpected;
	}

	public static String CheckTag(String host, String projectId, String expId, String scanId, String dicomTag, String tagValue, String j_session) throws IOException{
		String analysis = "";
		String dicomDumpURL = host + "/REST/services/dicomdump?src=/archive/projects/" + projectId + "/experiments/" + expId + "/scans/" + scanId + "&field=" +  dicomTag + "&format=JSON";
		//Get value returned by the URL; Check the value against tagValue
		ResponseEntity<String> response = GetResponse(dicomDumpURL,j_session);
		if (response.getStatusCode().equals(HttpStatus.OK)) {
			final String responseBody = response.getBody();
			if (responseBody!=null && responseBody.length()>0) {
				analysis = CheckValue(responseBody, tagValue);
			}
		}
		return analysis;
	}

	
	/*
	 * String dicomDumpURL = "XNAT_HOST_HERE/REST/services/dicomdump?src=/archive/projects/HCPEP-BWH-QC/experiments/PNL_E00001/scans/7&field=00181000&format=JSON"
	 * String tagValue = Dicom Field Value : 66109
	 * String user : XNAT User Name
	 * String password : XNAT Password
	 */
	

	public static String CheckTagRange(String host, String projectId, String expId, String scanId, String dicomTag, String tagValue,String j_session, String threshold) throws IOException{
		String analysis = "";
		String dicomDumpURL = host + "/REST/services/dicomdump?src=/archive/projects/" + projectId + "/experiments/" + expId + "/scans/" + scanId + "&field=" +  dicomTag + "&format=JSON";
		//Get value returned by the URL; Check the value against tagValue
		ResponseEntity<String> response = GetResponse(dicomDumpURL, j_session);
		if (response.getStatusCode().equals(HttpStatus.OK)) {
			final String responseBody = response.getBody();
			if (responseBody!=null && responseBody.length()>0) {
				analysis = CheckValue(responseBody, tagValue,threshold);
			}
		}
		return analysis;
	}


	private static String  CheckValue(String responseJSON, String tagV) throws IOException {
		String analysis = "";	
		boolean valueMatches = false;
		ObjectMapper mapper = new ObjectMapper();
		final JsonNode rootNode = mapper.readValue(responseJSON, JsonNode.class);
		final JsonNode resultSetNode = rootNode.get("ResultSet");
		final JsonNode resultNode = resultSetNode.get("Result");
		if (resultNode.isArray()) {
				if (resultNode.size() == 1) {
					final String tagValue = resultNode.get(0).get("value").asText();
					System.out.println("Tag Value: "+ tagValue + " Expected Value: " + tagV);
					valueMatches = tagV.equals(tagValue)?true:false; 
					analysis = valueMatches+COMPONENT_SEP+tagValue;
				}
			}
		return analysis;
	}

	private static String  CheckValue(String responseJSON, String dTagValue,String threshold) throws IOException {
		String analysis = "";
		String tagValue="";
		boolean valueMatches = true;
		try {
			ObjectMapper mapper = new ObjectMapper();
			final JsonNode rootNode = mapper.readValue(responseJSON, JsonNode.class);
			final JsonNode resultSetNode = rootNode.get("ResultSet");
			final JsonNode resultNode = resultSetNode.get("Result");
			float thresholdF = Float.parseFloat(threshold);
			if (resultNode.isArray()) {
					if (resultNode.size() == 1) {
						tagValue = resultNode.get(0).get("value").asText();
						//TagValue could be \ separated or a single float
						String[] valueParts = tagValue.split("\\\\");
						String[] dvalueParts = dTagValue.split("\\\\");
						if (valueParts.length != dvalueParts.length) {
						   System.out.println("Incorrect Arguments supplied to check " + tagValue + " " + dTagValue);	
						   analysis = "false" + COMPONENT_SEP + "Not Computed";
						   return analysis;
						}
						if (valueParts != null) {
							for (int i=0; i<valueParts.length; i++) {
								try {
									float iFloat = Float.parseFloat(valueParts[i]);
									float dTagValueF = Float.parseFloat(dvalueParts[i]);
									if (iFloat < (dTagValueF-thresholdF) || iFloat > (dTagValueF+thresholdF)) {
										valueMatches = false; break;
									}
									System.out.println("TagValue[" + i + "]: " + iFloat + " Expected: [" + (dTagValueF-thresholdF) +","+(dTagValueF+thresholdF)+"]");
								}catch(NumberFormatException nfe) {
									valueMatches = false;									
								}
							}
						}
					}
				}
		}catch(NumberFormatException nfe) {valueMatches = false;}
		analysis = valueMatches+ COMPONENT_SEP + tagValue ;
		return analysis;
	}

	
	private static ResponseEntity<String> GetResponse(String url, String user, String password) {
		final HttpEntity<?> httpEntity = new HttpEntity<Object>(GetAuthHeader(user, password));
		final SimpleClientHttpRequestFactory requestFactory =new SimpleClientHttpRequestFactory();
		final RestTemplate restTemplate = new RestTemplate(requestFactory);
		final ResponseEntity<String> response = restTemplate.exchange(url , HttpMethod.GET, httpEntity, String.class);
		return response;
	}

	
	private static ResponseEntity<String> GetResponse(String url, String j_session) {
		final HttpEntity<?> httpEntity = new HttpEntity<Object>(GetAuthHeader(j_session));
		final SimpleClientHttpRequestFactory requestFactory =new SimpleClientHttpRequestFactory();
		final RestTemplate restTemplate = new RestTemplate(requestFactory);
		final ResponseEntity<String> response = restTemplate.exchange(url , HttpMethod.GET, httpEntity, String.class);
		return response;
	}
	
	private static HttpHeaders GetAuthHeader(String j_session) {
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", "JSESSIONID=" + j_session);
		return headers;
	}

	private static 	HttpHeaders GetAuthHeader(String user, String password) {	
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + getBase64Credentials(user, password));
	    return headers;	
	}
	
	private static String getBase64Credentials(String user, String password) {
		final String plainCreds;
		plainCreds = user+":"+password;
		final byte[] plainCredsBytes = plainCreds.getBytes();
		final byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		final String base64Creds = new String(base64CredsBytes);
		return base64Creds;
	}
	
	public static void main(String[] args) {
		String host = args[0];
		String user = args[1];
		String password = args[2];
		String experiment =args[3];
		String scan = args[4];
		String project = args[5];
		String j_session = args[6];	
		String dicomTag = "00181020";
		String value = "syngo MR E11";
		try {
		boolean matches  = CheckTag(host,project, experiment,scan,dicomTag,value,user,password);
		System.out.println("Scanner Software Version " + matches + " " + matches);
		}catch(Exception e) {e.printStackTrace();}
	}

}
