package org.nrg.validation.dicom;

public interface XnatDicomValidator {
  public boolean isValid(String pathToDicomFiles);
}
