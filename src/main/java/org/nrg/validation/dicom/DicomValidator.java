package org.nrg.validation.dicom;

import java.io.File;
import java.util.List;

import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatResourcecatalogI;

public class DicomValidator extends XnatValidator {
		
		private static final String DICOM_LABEL = "DICOM";
		private static final String DICOM_RAW_CONTENT = "RAW";
		
		public static boolean CheckPrematureTermination(String pathToXML, String scanId, String validatorClassName) {
			boolean result = false;
			try {
				XnatImagesessiondataBean imageSession = GetImageSession(pathToXML);
				String dicomFilePath = GetDicomFilePath(imageSession, scanId);
	            if( validatorClassName!=null) {
	                Class cls = Class.forName(validatorClassName);
	                XnatDicomValidator dicomValidator = (XnatDicomValidator)cls.newInstance();
	                result  = dicomValidator.isValid(dicomFilePath);
	            }
			}catch(Exception e) {
				e.printStackTrace();
			}
			return result;
		}
		
		
		
		private static String GetDicomFilePath(XnatImagesessiondataBean imageSession, String scanId) {
			String path=null;
			XnatImagescandataI imageScan = GetScan(imageSession, scanId);
			if (imageScan!=null) {
				List<XnatAbstractresourceI> files = imageScan.getFile();
				for (XnatAbstractresourceI file:files) {
					if (file.getLabel()!=null && file.getLabel().equals(DICOM_LABEL)) {
						if (file instanceof XnatResourcecatalogI) {
							XnatResourcecatalogI resourceFile = (XnatResourcecatalogI)file;
							if (resourceFile.getContent() != null && resourceFile.getContent().equals(DICOM_RAW_CONTENT)) {
								String catalogPath = resourceFile.getUri();
								path = catalogPath.substring(0,catalogPath.lastIndexOf(File.separator));
							}
						}
					}
				}
			}
			return path;
		}
		
		private static XnatImagescandataI GetScan(XnatImagesessiondataBean imageSession, String scanId) {
			XnatImagescandataI scanReturn = null;
			List<XnatImagescandataI> scans = imageSession.getScans_scan();
			for (XnatImagescandataI scan:scans) {
				if (scan.getId().equals(scanId)) {
					scanReturn = scan;
					break;
				}
			}
			return scanReturn;
		}
	
}
